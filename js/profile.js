$(function(){
'use strict';
    // submitボタンは非表示
    $('#submit').hide();

	// ツールチップの表示
    $('[data-toggle="tooltip"]').tooltip()

	//  edit_tagが押されたら
	$('#edit_tag').on('click', function() {
		$('.d-none').removeClass(); //d-noneのチェックボックスを表示する
        $('#submit').show(); //submitボタンを表示
        $('#submit').on('click', function() {
            $('.d-none').addClass(); // チェックボックスを非表示に
            $('form').submit();
            alert('タグを変更しました。');
        })
	});


});
