<?php

ini_set('display_errors', 1);

// セッションの有効期限
ini_set('session.gc_maxlifetime', 60 * 60 * 5);

// 本当は別ファイルにしてgitignoreする！
define('DB_HOST', 'localhost');
define('DB_NAME', 'creche_app');
define('DB_CHAR', 'utf8');
define('DB_USERNAME', 'dbuser01');
define('DB_PASSWORD', '1234');

define('DB_DSN',
	'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHAR);

define('SITE_URL', 'http://' . $_SERVER['HTTP_HOST']);

require_once(__DIR__ . '/../lib/functions.php');
require_once(__DIR__ . '/../lib/Index.php');
// require_once(__DIR__ . '/../lib/Memberlist.php');
require_once(__DIR__ . '/../lib/EnterSlacktoken.php');
require_once(__DIR__ . '/../lib/Profile.php');


session_start();
