<?php

require_once(__DIR__ . '/config/config.php');

$app = new MyApp\EnterSlacktoken();

 ?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset='utf-8'>
    <title>Crèche（クレイシュ） - メンバーリスト</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>

<body>
<div class="wrapper mx-auto mt-5 bg-white p-5">


    <header id='header'>
        <div class="cover">
            <h1>Crèche（クレイシュ） メンバーリスト</h1>
        </div>
    </header>


    <main>
        <form method="post" action="" class="my-5">
            <label>あなたのトークンを入力してください。</label><br>
            <input type="text" name="slacktoken" class="w-50">
            <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
            <input type="submit" value="GO!" class="btn btn-primary">

            <?php if ($_SESSION['err']): ?>
                <?php foreach ((array)$_SESSION['err'] as $err): ?>
                    <p class="text-danger"><?= h($err); ?></p>
                <?php endforeach; ?>
            <?php endif; ?>

            <p>トークンは<a href="https://api.slack.com/custom-integrations/legacy-tokens" target="_blank">こちら</a>から取得できます。</p>
        </form>

    </main>

    <footer id='footer'>

    </footer>

</div><!-- container -->

<!-- jQuery, Popper.js, Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
