<?php

require_once(__DIR__ . '/config/config.php');

$data = new \MyApp\Profile();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    echo 'テスト';
    $data->post();
}
 ?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset='utf-8'>
    <title>Crèche - （）さんのプロフィール</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/profile.css">
</head>

<body>
<div class="wrapper mx-auto mt-5 bg-white p-5" style="max-width:900px;">


    <header id='header'>
        <p class="float-right"><a href="/" class="btn btn-primary">一覧に戻る</a></p>
        <h1>プロフィール</h1>
    </header>

    <main>
        <div class="container">
            <div class="row">
                <div id="username" class="box order-sm-2 col-md-8">
                    <label>名前　<i class="fas fa-edit"></i></label>
                    <p><?= h($data->getUserName());?></p>
                </div>

                <div id="usericon" class="order-sm-1 text-center col-md-4">
                    <img style="width:192px;" alt="ユーザーアイコン" class="rounded" src="<?= h($data->getUserIcon());?>">
                </div>
            </div>

            <div id="usertag" class="box">
                <label>タグ
                    <a href="#" id="edit_tag" data-toggle="tooltip" title="編集する">
                        <i class="fas fa-edit"></i>
                    </a>
                </label>
                <div class="tags">
                    <form action="" method="POST">
                        <p><?= $data->getTagsUserHas();?></p>
                        <a href="" name="submit" id="submit">
                            <span class="btn btn-primary">OK</span>
                        </a>
                    </form>
                </div>
            </div>

            <div id="userblog" class="box">
                <label>blog　<i class="fas fa-edit"></i></label>
                <p class="inline-block">http://www.example.com/example</p>
            </div>

        </div>


    </main>

    <footer id='footer'>

    </footer>

</div><!-- container -->

<!-- jQuery, Popper.js, Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="js/profile.js">

</script>
</body>
</html>
