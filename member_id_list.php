<?php

//アクセスがPOSTだったら
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['token']))
{
    $token = trim($_POST['token']);

    // tokenが正しい形式か判定する
    $prefix = 'xoxp-';
    if (strpos($token, $prefix, 0) === false)
    {
        $err = "ごめんなさい、これは正しいtokenではないみたいです。";
    }
    else
    {
        $methodURL = "https://slack.com/api/users.list";
        $option = "&presence=false&pretty=1";
        // JSONデータ取得用URL
        $jsonURL = $methodURL . "?token=" . $token . $option;

        // JSONデータを格納
        $jsonDATA = file_get_contents($jsonURL);

        // 読み取ったデータの文字化け防止（自動的に検出しUTF-8に変換）
        $jsonDATA = mb_convert_encoding($jsonDATA, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

        // JSONデータを連想配列にする
        $jsonDATA = json_decode($jsonDATA, true);

        $body = "";
		$memberNum = count($jsonDATA['members']); //現在のメンバー数
        foreach ($jsonDATA['members'] as $key => $member)
        {
            if ($member['deleted'] === true)
            {
                $deletedUser++;
            }  else if ($member['profile']['display_name'] !== '')
			{
				$name = $member['profile']['display_name'];
			} else
			{
				$name = $member['profile']['real_name'];
			}
        $slackId = $member['id'];
        $body .= "$slackId" . "\t" . $name . "<br>";
        }
    }
}

function h($s)
{
  return htmlspecialchars($s, ENT_QUOTES, 'utf-8');
}


 ?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset='utf-8'>
    <title>Crèche（クレイシュ） - メンバーリスト</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">


</head>

<body class="bg-light">
<div class="wrapper mx-auto mt-5 bg-white p-5" style="max-width:900px;">


    <header id='header'>
        <h1>Crèche（クレイシュ） メンバーID一覧</h1>
    </header>


    <main>
        <form method="post" action="" class="mb-5">
            <label >あなたのトークンを入力してください。</label><br>
            <input type="text" name="token" class="w-50">
            <input type="submit" value="GO!" class="btn btn-primary">
                <?php if ($err): ?>
                    <p class="text-danger"><?= h($err); ?></p>
                <?php endif; ?>
            <p>トークンは<a href="https://api.slack.com/custom-integrations/legacy-tokens" target="_blank">こちら</a>から取得できます。</p>
        </form>

        <h2>メンバー一覧（<?= h($memberNum); ?>人）</h2>

                <?php if (!empty($body)): ?>
                    <?= ($body);?>
                <?php endif; ?>

    </main>


</div><!-- container -->
