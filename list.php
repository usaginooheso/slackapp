<?php

require_once(__DIR__ . '/config/config.php');

$_SESSION['err'] = '';

if (empty($_SESSION['me']))
{
    header('Location: ' . SITE_URL . '/enter.php');
} else
{
        $slacktoken = trim($_SESSION['me']);

        // tokenが正しい形式か判定する
        $prefix = 'xoxp-';
        if (strpos($slacktoken, $prefix, 0) === false)
        {
            $_SESSION['err'] = "ごめんなさい、これは正しいtokenではないみたいです。";
            header('Location: index.php');
            exit;
        }
        else
        {
            $methodURL = "https://slack.com/api/users.list";
            $option = "&presence=false&pretty=1";
            // JSONデータ取得用URL
            $jsonURL = $methodURL . "?token=" . $slacktoken . $option;

            // JSONデータを格納
            $jsonDATA = file_get_contents($jsonURL);

            // 読み取ったデータの文字化け防止（自動的に検出しUTF-8に変換）
            $jsonDATA = mb_convert_encoding($jsonDATA, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

            // JSONデータを連想配列にする
            $jsonDATA = json_decode($jsonDATA, true);

            $body = "";
            $memberNum = count($jsonDATA['members']); //現在のメンバー数
            $deletedUser = 0;   //削除済みユーザ
            $otherUser = 0; //GoogleDriveとslackbotの除外用

            foreach ($jsonDATA['members'] as $key => $member)
            {
                if ($member['deleted'] === true)
                {
                    $deletedUser++;
                } else if ($member['profile']['display_name'] === 'Google Drive')
                {
                    $otherUser++;
                } else if ($member['profile']['display_name'] === 'slackbot')
                {
                    $otherUser++;
                } else
                {
                    if ($member['profile']['display_name'] !== '')
                    {
                        $name = $member['profile']['display_name'];
                    } else
                    {
                        $name = $member['profile']['real_name'];
                    }
                    $body .= "<tr>";
                    $body .= "<td><img src=\"" . $member['profile']['image_48'] . "\"></td>";
                    $body .= "<td>" . $name . "</td>";
                    $body .= "<td>" . "</td>";
                    $body .= "<tr>";
                }
            }
        }

    // 非アクティブなユーザーを除外
    $memberNum = $memberNum - $deletedUser - $otherUser;
}


 ?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset='utf-8'>
    <title>Crèche - メンバーリスト(Slackデータ)</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">


</head>

<body class="bg-light">
<div class="wrapper mx-auto mt-5 bg-white p-5" style="max-width:900px;">


    <header id='header' class="mb-5">
        <div class="cover">
            <h1>Crèche メンバーリスト(Slackデータ)</h1>
        </div>
    </header>


    <main>
        <h2>メンバー一覧（<?= h($memberNum); ?>人）</h2>
        <table class="mb-5 table table-hover text-center">
            <thead class="">
                <tr>
                    <th class="text-center">icon</th>
                    <th>Display Name</th>
                    <th>tags</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($body)): ?>
                    <?= ($body);?>
                <?php endif; ?>
            </tbody>
        </table>
    </main>

    <footer id='footer'>

    </footer>

</div><!-- container -->

<!-- jQuery, Popper.js, Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
