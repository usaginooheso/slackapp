<?php

namespace MyApp;

class Profile
{
	private $_dbh;
	private $_id;
	private $_slackId;
	private $_userName;
	private $_userIcon;
	private $_allTags; //タグ名（全種類）
	private $_tagsUserHas; //ユーザの持ってるタグ

	public function __construct()
	{
		// me が空だったらenter.phpに戻す
		if(empty($_SESSION['me']))
		{
			header('Location: '. SITE_URL . '/enter.php');
			exit;
		} else {
			// GETのパラメータからidを取得
			if ($_SERVER['REQUEST_METHOD'] === 'GET') {
				$this->_id = $_GET['id'];
			}
			try {
				$this->_dbh = new \PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
				$this->_dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$this->_setSlackId();
				$this->_setUserName();
				$this->_setTagNames();
				$this->_setUserTag();
				$this->_setUserIcon();
			} catch (\Exception $e) {
				echo $e->getMessage();
				exit;
			}
		}
	}

	public function post() {
		echo 'どれどれ？';
		try {
			var_dump($_POST);
			// validate
			// save
		} catch (\Exception $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getUserIcon() {
		return $this->_userIcon;
	}

	private function _setUserIcon() {
		$methodURL = "https://slack.com/api/users.profile.get";
		$slackId = $this->_slackId;
		$option1 = '&user=';
		$option2 = "&pretty=1";
		// JSONデータ取得用URL
		$jsonURL = $methodURL . "?token=" . $_SESSION['me'] . $option1 . $slackId . $option2;

		// JSONデータを格納
		$jsonDATA = file_get_contents($jsonURL, 'UTF-8');

		// 読み取ったデータの文字化け防止（自動的に検出しUTF-8に変換）
		$jsonDATA = mb_convert_encoding($jsonDATA, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// JSONデータを連想配列にする
		$jsonDATA = json_decode($jsonDATA, true);

		$this->_userIcon = $jsonDATA['profile']['image_192'];
	}

	public function getTagsUserHas() {

		// ユーザの持ってるタグだけを番号で保持している場合
		// spanタグでタグを出力する
		// $list = ''; //出力用の変数
		// if (count($this->_tagsUserHas)) { //ユーザーが1つ以上タグを持っていたら
		// 	foreach ($this->_tagsUserHas as $tag) { //ユーザーの持ってるタグを1津ずつ取り出して
		// 		$list .= '<span class="">#' . $tag . '</span>';
		// 	}
		// }

		// ユーザがタグを持っているかどうか true/false で保持されている場合
		$list = ''; //出力用の変数
		for ($i = 1; $i <= count($this->_allTags); $i++ ) {
			$j = $i-1;
			if ($this->_tagsUserHas[$i] === true) {
				$list .= '<input type="checkbox" class="tag d-none" checked="checked" value="';
				$list .= $this->_allTags[$j]['id'] . '">';
				$list .= '<span class="">#' . $this->_allTags[$j]['name'] . '</span>';


			} else {
				$list .= '<input type="checkbox" class="tag d-none" value="' . $this->_allTags[$j]['id'] . '">';
				$list .= '<span class="d-none">' . $this->_allTags[$j]['name'] . '</span>';
			}
		}

		// echo $list;
		return $list;
	}

	private function _setUserTag() {
		// ユーザーについているタグ（id）を取得
		$sql = 'SELECT tag_id FROM user_tags WHERE user_id =' . $this->_id . ';';
		$stmt = $this->_dbh->query($sql);
		$tagNumberUserHas = $stmt->fetchAll(\PDO::FETCH_COLUMN);
		// var_dump($tagNumberUserHas);
		// exit;

		$i = 0;
		$arr = $this->_allTags; //idとnameの配列
		// var_dump($arr);
		// exit;

		// ユーザの持ってるタグだけを番号で保持する
		// すべてのタグ名をループで回して
		// for ($i = 0; $i < count($arr); $i++) {
		// 	// var_dump($arr[$i]['id']);
		// 	// var_dump($tagNumberUserHas);
		// 	// ユーザーの持ってるタグidを1つずつ取り出して
		// 	for ($j = 0; $j < count($tagNumberUserHas); $j++) {
		// 		//タグのno と ユーザの持ってるタグのnoが同じだったら
		// 		if ($arr[$i]['id'] === $tagNumberUserHas[$j]) {
		// 			$this->_tagsUserHas[] =  $arr[$i]['name'];
		// 		}
		// 	}
		// }

		// 全タグについて、ユーザーが持ってるかどうかをtrue/falseで保持する
		$this->_tagsUserHas[0] = ''; //0は埋めておく
		// すべてのタグ名をループで回して
		for ($i = 0; $i < count($arr); $i++) { //6回ループ

			// // ユーザーの持ってるタグidを1つずつ取り出して
			foreach ($tagNumberUserHas as $tag) {
				//タグのno と ユーザの持ってるタグ(no)が同じだったら
				if ($arr[$i]['id'] === $tag) {
					// echo "タグ" . $arr[$i]['id'] . "を持っています<br>";
					$this->_tagsUserHas[] = true;
					continue 2;

				}
			}
			// echo "タグ" . $arr[$i]['id'] . "は持っていません<br>";
			$this->_tagsUserHas[] = false;
		}
		// var_dump($this->_tagsUserHas);
	}

	// タグ全種類を取得
	private function _setTagNames() {
		$sql = 'SELECT * FROM tags;';
		$stmt = $this->_dbh->query($sql);
		$tags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->_allTags = $tags;
		// var_dump($this->_allTags);
		// exit;
	}

	public function getUserName() {
		return $this->_userName;
	}

	private function _setUserName() {
		$sql = 'SELECT name FROM users WHERE id=' . $this->_id . ';';
		$stmt = $this->_dbh->query($sql);
		$this->_userName = $stmt->fetch(\PDO::FETCH_COLUMN);
		// var_dump($this->_userName);
	}

	private function _setSlackId() {
		$sql = "SELECT slack_id FROM users WHERE id=" . $this->_id . ";";
		$stmt = $this->_dbh->query($sql);
		$slackId = $stmt->fetch(\PDO::FETCH_ASSOC);
		$this->_slackId = $slackId['slack_id'];
	}


}
