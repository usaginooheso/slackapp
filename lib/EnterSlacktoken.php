<?php

namespace MyApp;

class EnterSlacktoken
{
	public function __construct()
	{
		$_SESSION['me'] = '';
		
		$this->_createToken();
		$_SESSION['err'] = '';

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			try
			{
				$this->_validateToken();
				$this->_validateSlackToken();
				header('Location: ' . SITE_URL . '/index.php');
				exit;
			} catch (\Exception $e)
			{
				$_SESSION['err'] = $e->getMessage();
			}
		}

	}

	private function _createToken()
	{
		if (empty($_SESSION['token']))
		{
			$_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));
		}
	}

	private function _validateSlackToken()
	{
		if (empty($_POST['slacktoken']))
	    {
	        throw new \Exception('トークンを入力してくださいっ!');
	    } else
		{
			$slacktoken = trim($_POST['slacktoken']);
	        $prefix = 'xoxp-';
	        if (strpos($slacktoken, $prefix, 0) === false)
	        {
				throw new \Exception('ごめんなさい、これは正しいtokenではないみたいです。');
	        } else {
	        	// バリデーションを通ったら
				$_SESSION['me'] = $slacktoken;
				$this->_setLoginUsersData();
	        }

		}
	}

	// ログインした（＝slacktokenを入力した本人）の名前を取得
	private function _setLoginUsersData() {
		$methodURL = "https://slack.com/api/users.profile.get";
		$option = "&pretty=1";

		// JSONデータ取得用URL
		$jsonURL = $methodURL . "?token=" . $_SESSION['me'] . $option;

		// JSONデータを格納
		$jsonDATA = file_get_contents($jsonURL);

		// 読み取ったデータの文字化け防止（自動的に検出しUTF-8に変換）
		$jsonDATA = mb_convert_encoding($jsonDATA, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// JSONデータを連想配列にする
		$jsonDATA = json_decode($jsonDATA, true);

		$loginUser = $jsonDATA['profile'];
		if ($loginUser['display_name'] !== '') {
			$_SESSION['name'] = $loginUser['display_name'];
		} else {
			$_SESSION['name'] = $loginUser['real_name'];
		}
		$_SESSION['icon'] = $loginUser['image_48'];
	}

	private function _validateToken()
	{
		if (
			empty($_SESSION['token']) ||
			empty($_POST['token']) ||
			$_SESSION['token'] !== $_POST['token']
		) {
			throw new \Exception('invalid token!');
		}
	}
}
