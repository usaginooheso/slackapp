<?php

namespace MyApp;

class Index
{
	private $_db;
	private $_body;
	private $_memberNum;
	private $_tagNames;
	private $_userIcon;

	public function __construct()
	{
		// $_SESSION['me'] = '';
		// me が空だったらenter.phpに戻す
		if(empty($_SESSION['me']))
		{
			header('Location: '. SITE_URL . '/enter.php');
			exit;
		} else {
			try {
				$this->_db = new \PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
				$this->_db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$this->_setTagNames();
				$this->_setMemberIconFromSlack();
				$this->_setMemberList();
			} catch (\Exception $e) {
				echo $e->getMessage();
				exit;
			}
		}
	}

	private function _setMemberList() {
		// ユーザーリストを配列で得る
		$stmt = $this->_db->query('SELECT id, name FROM users');
		$users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		// タグがついてるユーザーのIDとタグIDを配列で得る
		$stmt = $this->_db->query('SELECT user_id, tag_id FROM user_tags;');
		$tagids = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($users as $user) {
			$this->_body .= "<tr>";
			$this->_body .= "<td>" . $user['id'] ."</td>";
			$this->_body .= "<td><img class='iconOfMembers rounded' src='";
			$this->_body .= $this->_userIcon[$user['id']];
			$this->_body .= "'></td>";
			$this->_body .= "<td>" . $user['name'] ."</td>";
			$this->_body .= "<td><a href='profile.php?id=" . $user['id'] ."'>プロフィール</a></td>";

			// タグidからタグ名を得る
			$this->_body .= "<td>";
			foreach ($tagids as $tag) {
				// ユーザーにタグがついていたら
				if ($tag['user_id'] === $user['id']) {
					// タグ欄にタグIDを表示
					$num = $tag['tag_id'];
					// var_dump($this->_tagNames[$num]);
					$this->_body .= "#" . $this->_tagNames[$num]. " ";
				}
			}
			$this->_body .= "</td>";
			$this->_body .= "</tr>";
		}

		// メンバーの人数
		$this->_memberNum = count($users);
	}

	// アイコンをSlackから取ってくる
	// ここはもっといいやり方があるかも
	private function _setMemberIconFromSlack() {
		$methodURL = "https://slack.com/api/users.list";
		$option = "&presence=false&pretty=1";
		// JSONデータ取得用URL
		$jsonURL = $methodURL . "?token=" . $_SESSION['me'] . $option;

		// JSONデータを格納
		$jsonDATA = file_get_contents($jsonURL);

		// 読み取ったデータの文字化け防止（自動的に検出しUTF-8に変換）
		$jsonDATA = mb_convert_encoding($jsonDATA, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');

		// JSONデータを連想配列にする
		$jsonDATA = json_decode($jsonDATA, true);

		$memberNum = count($jsonDATA['members']); //現在のメンバー数
		$deletedUser = 0;   //削除済みユーザ
		$otherUser = 0; //GoogleDriveとslackbotの除外用

		$this->_userIcon[0] = '';
		foreach ($jsonDATA['members'] as $key => $member)
		{
			if ($member['deleted'] === true)
			{
				// $deletedUser++;
				continue;
			} else if (
				$member['profile']['display_name'] === 'Google Drive' ||
				$member['profile']['display_name'] === 'slackbot'
			){
				// $otherUser++;
				continue;
			} else
			{
				$this->_userIcon[] = $member['profile']['image_48'];
			}
		}
	}

	// タグ全種類を取得
	private function _setTagNames() {
		$stmt = $this->_db->query('select * FROM tags;');
		$tagNames = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
		$this->_tagNames = $tagNames;
		// var_dump($this->_tagNames);
	}

	public function getMemberNum(){
		return $this->_memberNum;
	}

	public function getMemberList() {
		return $this->_body;
	}
}
